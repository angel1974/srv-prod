package com.techuniversity.prod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

//import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

//le decimos que esta clase va a actuar como servicio
@Service
public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }
    //optional devuelve un objeto para gestionar si lo ha encontrado o no
    public Optional<ProductoModel> findByid(String id){
        return productoRepository.findById(id);
    }
    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
        //save sirve para insert y para update
    }
    public boolean deleteProducto(ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    //metodo que devuelve lista de productos, pero se solicita que aporte un numero de pagina
    public List<ProductoModel> findPaginado(int page) {
        Pageable pageable = PageRequest.of(page, 3);
        Page<ProductoModel> pages = productoRepository.findAll(pageable);
        List<ProductoModel> productos = pages.getContent();
        return productos;
    }
}
