package com.techuniversity.prod.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

//interfaz que hereda de un repositorio Mongo
@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel,String> {

}
