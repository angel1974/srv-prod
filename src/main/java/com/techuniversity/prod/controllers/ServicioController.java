package com.techuniversity.prod.controllers;

import com.techuniversity.prod.servicios.ServiciosService;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/servicios")
public class ServicioController {

//    @GetMapping("/servicios")
//    public List getServicios(){
//        return ServiciosService.getAll();
//    }
    @PostMapping("/servicios")
    public String setServicios(@RequestBody String cadena){
        try {
            Document doc = Document.parse(cadena);
            List<Document> lstServicios = doc.getList("servicios", Document.class);
            if (lstServicios != null ) {
                ServiciosService.insertBatch(cadena);
            } else {
                ServiciosService.insert(cadena);
            }

            return "OK";
        } catch (Exception ex){
            return ex.getMessage();
        }
    }

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro) {
        return ServiciosService.getFiltrados(filtro);
    }

    @GetMapping("/servicios/periodo")
    public List getServiciosPeriodo(@RequestParam String periodo) {
        //String json = String.format("\disponibilidad\")
        Document doc=new Document();
        doc.append("disponibilidad.periodos", periodo);

        return ServiciosService.getFiltrados(doc);
    }

    @PutMapping("/servicios")
    public String upServicios(@RequestBody String data) {
        try {
            JSONObject obj = new JSONObject(data);
            String filtro =obj.getJSONObject("filtro").toString();
            String valores =obj.getJSONObject("valores").toString();
            ServiciosService.update(filtro,valores);
            return "OK";
        } catch (Exception ex){
            return ex.getMessage();
        }
    }
}
